# Introduction to Data Mining - REI502M

This is a collection of notebooks, codes and other work related to the course:
[*Introduction to Data Mining*](https://ugla.hi.is/kennsluskra/index.php?sid=&tab=nam&chapter=namskeid&id=70855320196), taught in Autumn 2019.



Here is a [solution](https://notendur.hi.is/~els11/REI502M/Homework01_solutions_Elias.pdf) to the first homework set in the course.
